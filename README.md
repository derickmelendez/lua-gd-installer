# lua-gd-installer

After many headaches I finally found a way to compile the lua-gd library from source in Ubuntu 22.04 LTS so I could use some of the FCEUX Emulator's fuinctions, hopefully this can save someone further headaches.

*Make sure you have lua dev installed for the compilation to work*

`sudo apt install liblua5.3-dev`

Or change the number to match your lua version

After installation you can test it by running the following lua code

```
local gd = require('gd')
local im = gd.createTrueColor(50,50)
im:png('image.png')
```

The output should be a 50x50 black png
