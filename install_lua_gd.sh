#!/usr/bin/env bash

#Slight modification of https://github.com/ehrenbrav/DeepQNetwork/issues/2#issuecomment-529354308
lua_ver=5.1

echo "Installing Lua-GD ... "
echo "*****Make sure you have lua dev installed for the compilation to work*****"
echo "sudo apt install liblua5.3-dev"
echo "Or change the number to match your lua version"
rm -rf gd-src
mkdir gd-src
cd gd-src
git clone https://github.com/ittner/lua-gd.git
cd lua-gd
#Change lua5.1 to your version of lua
#This changes both the packge version and the binary name, if your binary name is not lua5.3
#manually change LUABIN=lua5.1 to equal whatever your command line recognizes as the lua command
sed -i "s/lua5.1/lua$lua_ver/" Makefile
#Need to run as root to install and run luarocks and GD dev headers
apt -y install luarocks libgd-dev
luarocks --lua-version $lua_ver make
RET=$?; if [ $RET -ne 0 ]; then echo "Error. Exiting."; exit $RET; fi
echo "Lua-GD installation completed"

echo
echo "All done!"

echo "If the installation completed successfully you can try the following lua code to test it"
echo "local gd = require('gd'); local im = gd.createTrueColor(50,50); im:png('image.png')"
echo "The output should be a 50x50 black png"
